#!/bin/bash

make &&
echo "----------------------------------------------------" &&
./markdowntotex data.txt &&
echo "----------------------------------------------------" &&
pdflatex out.tex &&
echo "----------------------------------------------------" &&
rm -f out.out out.log out.aux
#evince out.pdf &
