This project is currently not under active development.

# Convert a custom markdown to .tex


## TODO
* math
  * greek symbols
  * brackets
  * set operations: union, ...
  * sets: R, N, ...
  * infinity
  * text: ""
* lists * 123
* citation >
* text formatting: underline, linethrough, code
* code block


## DONE
* basic stuff
* double newline
* Umlaute öäüß
* newline on double space at the end
* other headings
* bold, italic, ...
* basic latex math
* math
  * sum from to
  * prod from to
  * lim from
  * frac, sfrac, cfrac
  * latex only math
  * arrows
  * forall, in exists
  * cos, sin
* disable text formatting
* file watcher: recompile to pdf on save

## Install

Make sure you have the `golang`-package installed. The shell scripts use `pdflatex` to build the pdf.

clone from gitlab:

    git clone https://gitlab.com/JonasLoos/markdowntotex.git

Move into project folder (`cd markdowntotex`) and run:

    make

now you can run the program with:

    ./markdowntotex <inputFile> [<outputFile>]

To watch the file to autocompile on save use:

    ./file_watcher <inputFile> <outputFileBasename>

To clean up the latex build mess afterwards use:

    ./clean <outputBasename>

example:

    git clone https://gitlab.com/JonasLoos/markdowntotex.git
    cd markdowntotex
    make
    ./markdown myFirstFile.md myOutputFile.tex
    ./file_watcher myOutputFile.tex
    # write your stuff
    ./clean myOutputFile

## Usage

### Markdown

**Headings**: `#`, `##`, `###`

**Text Formatting**:

it's not allowed to span across multiple lines

* italic: `* ... *`
* bold: `** ...**`

disable text formatting with a `!` at the beginnning of the line, to start a line with `!` use `!!`

**Newlines**:
* single newline: double space `  ` at the end
* double newline: double newline

**Math**:
* inline: `$ ... $`
* single equation: `$$ ... $$`


### Math

`!` at the beginning of an eqation diables abbreviations (so latex only).

Supported syntax (in order of execution):

    inf
    sum from <a> to <b>
    prod from <a> to <b>
    int from <a> to <b>
    lim from a
    frac <a> <b>
    cfrac <a> <b>
    sfrac <a> <b>
    sqrt a
    root <a> <b>
    ->
    =>
    <=>
    ==>
    <==>
    <=
    >=
    !=
    +-
    -+
    times
    forall
    exists
    and
    or
    sin
    cos
    tan

with `<a>`, `<b>` beeing expressions without spaces.
