CC = go build


all: markdowntotex

clean: 
	rm -f markdowntotex

markdowntotex: markdowntotex.go
	$(CC) markdowntotex.go
