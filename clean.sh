#!/bin/bash

if [ $# -ne 1 ]
  then
    echo "Usage: $0 <FileBasename>"
    echo "Example: $0 out"
    echo "removes out.aux, out.out, out.log, out.tex"
    exit 1
fi

rm -rf $1.aux $1.out $1.log $1.tex
