package main

import (
    "fmt"
    "io/ioutil"
    "os"
    "strings"
    "regexp"
)

const latexStart = `
\documentclass{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[latin1]{inputenc}

\begin{document}
`
const latexEnd = "\n\\end{document}\n"

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func regex(text, r, alt string) string {
    return regexp.MustCompile(r).ReplaceAllString(text, alt)
}

func textFormatting(t string) string {
    // disabled formatting with !, ! at the beginning with !!
    if t[0] == '!' {
        if t[1] != '!' {
            return t[1:]
        }
        t = t[1:]
    }

    // bold and italic
    t = regex(t, `([^\\\*])\*{3}(.+)([^\\\*])\*{3}`, `$1\textbf{\textit{$2}}$3`)
    // bold
    t = regex(t, `([^\\\*])\*{2}(.+)([^\\\*])\*{2}`, `$1\textbf{$2}$3`)
    // italic
    t = regex(t, `([^\\\*])\*{1}(.+)([^\\\*])\*{1}`, `$1\textit{$2}$3`)

    return t
}

func mathReplacer(t string) string {
    // latex math only
    if t[2] == '!' {
        return "$$" + t[3:]
    }

    // infinity
    t = regex(t, ` inf `, `\infty `)
    // sum
    t = regex(t, ` sum( from (\S+))?( to (\S+))?`, ` \sum_{$2}^{$4}`)
    // prod
    t = regex(t, ` prod( from (\S+))?( to (\S+))?`, ` \prod_{$2}^{$4}`)
    // int
    t = regex(t, ` int( from (\S+))?( to (\S+))?`, ` \int_{$2}^{$4}`)
    // lim
    t = regex(t, ` lim( from (\S+))?`, ` \lim\limits_{$2}`)
    // frac
    t = regex(t,  ` frac (\S+) (\S+)`, ` \frac{$1}{$2}`)
    t = regex(t, ` cfrac (\S+) (\S+)`, ` \cfrac{$1}{$2}`)
    t = regex(t, ` sfrac (\S+) (\S+)`, ` {{}^{$1}\!/_{$2}}`)
    // roots
    t = regex(t, ` sqrt (\S+)`, `\sqrt{$1}`)
    t = regex(t, ` root (\S+) (\S+)`, `\sqrt[$1]{$2}`)
    // arrows
    t = regex(t, `->`, ` \to `)
    t = regex(t, ` => `, ` \Rightarrow `)
    t = regex(t, ` <=> `, ` \Leftrightarrow `)
    t = regex(t, ` ==> `, ` \Longrightarrow `)
    t = regex(t, ` <==> `, ` \Longleftrightarrow `)
    // equals & co (after arrows to not destroy em)
    t = regex(t, `<=`, `\leq `)
    t = regex(t, `>=`, `\geq `)
    t = regex(t, `!=`, `\neq `)
    // other operators
    t = regex(t, `\+-`, `\pm `)
    t = regex(t, `-\+`, `\mp `)
    t = regex(t, ` times `, `\times `)
    t = regex(t, ` forall `, `\forall `)
    t = regex(t, ` exists `, `\exists `)
    t = regex(t, ` and `, `\land `)
    t = regex(t, ` or `, `\lor `)
    // cos sin stuff
    t = regex(t, ` sin `, `\sin `)
    t = regex(t, ` cos `, `\cos `)
    t = regex(t, ` tan `, `\tan `)


    return t
}

func main() {

    // input file
    if len(os.Args) < 2 {
        fmt.Println("filename missing")
        return
    }
    inFile := os.Args[1]

    // output file
    outFile := "out.tex"
    if len(os.Args) > 2 {
        outFile = os.Args[2]
    }

    // read inputFile
    dat, err := ioutil.ReadFile(inFile)
    check(err)
    umlauteReplacer := strings.NewReplacer(
    "ä", `\"a`,
    "Ä", `\"A`,
    "ü", `\"u`,
    "Ü", `\"U`,
    "ö", `\"o`,
    "Ö", `\"O`,
    "ß", `\ss{}`)
    arr := strings.Split(umlauteReplacer.Replace(string(dat[:])), "\n")

    // parse input
    res := latexStart
    lastEmpty := false
    parStarted := false

    for i := 0; i < len(arr); i++ {
        // empty line
        if len(strings.TrimSpace(arr[i])) == 0 {
            lastEmpty = true
            continue
        }
        // header (before nl to prevent it)
        if arr[i][0] == '#' {
            res += "\n\\"
            if arr[i][1] == '#' {
                res += "sub"
                if arr[i][2] == '#' {
                    res += "sub"
                }
            }
            res += "section{"+textFormatting(strings.TrimSpace(strings.TrimLeft(arr[i][1:], "#")))+"}\n"
            lastEmpty = false
            parStarted = true
            continue
        }
        // double newline
        if lastEmpty {
            lastEmpty = false
            if !parStarted {
                res += "\\newline \\newline\n"
            }
        }
        // text
        tmp := textFormatting(arr[i])
        tmp = regexp.MustCompile(`\$.+\$|\$\$.+\$\$`).ReplaceAllStringFunc(tmp, mathReplacer)
        res += strings.TrimSpace(tmp) + " "
        parStarted = false
        // single newline (double space)
        if len(arr[i]) > 2 && arr[i][len(arr[i])-2:] == "  " {
            res += "\\newline\n"
        }

    }

    res += latexEnd

    // write output
    check(ioutil.WriteFile(outFile, []byte(res), 0644))
}
