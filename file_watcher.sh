#!/bin/bash

if [ $# -ne 2 ]
  then
    echo "Usage: $0 <inputFile> <outputFileBasename>"
    echo "Example: $0 myFile.md myOutput"
    echo "watches myFile.md and creates myOutput.pdf"
    exit 1
fi

# first run
./markdowntotex $1 $2.tex &&
pdflatex -output-directory $(dirname $2) $2.tex
evince $2.pdf &

### Set initial time of file
LTIME=`stat -c %Z $1`

while true
do
   ATIME=`stat -c %Z $1`

   if [[ "$ATIME" != "$LTIME" ]]
   then    
       echo "file changed!"
       ./markdowntotex $1 $2.tex &&
       pdflatex -output-directory $(dirname $2) $2.tex
       LTIME=$ATIME
   fi
   sleep 0.2
done
